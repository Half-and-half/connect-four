from tkinter import *
from tkinter import ttk


class GameBoardGUI:
    #Gui constructor hold view info
    def __init__(self):
        self.numRows = 6
        self.numCols = 7
        self.count = 1
        # Build a 2D matrix of a given size filled with all zeros
        self.board = [['0' for col in range(self.numCols)] for row in range(self.numRows)]

        self.root = Tk()
        self.root.title("Connect4")

        self.mainframe = ttk.Frame(self.root, padding="3 3 12 12")
        self.mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
        self.mainframe.columnconfigure(0, weight=1)
        self.mainframe.rowconfigure(0, weight=1)

        self.indrop = StringVar()

        self.drop_entry = ttk.Entry(self.mainframe, width=15, textvariable=self.indrop)
        self.drop_entry.grid(column=3, row=4, sticky=(W))

        ttk.Button(self.mainframe, text="Drop Piece", command=self.drop_Piece).grid(column=3, row=5, sticky=W)
        ttk.Label(self.mainframe, text="Select Column").grid(column=3, row=3, sticky=W)
        for child in self.mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)


    def drop_Piece(self, *args):
        if(self.count % 2 == 1):
            player = 'A'
        else:
            player = 'B'

        try:
            column = (int(self.indrop.get())-1)
            found = False
            # change the value of the board at position column[j][i]
            if column < self.numCols:
                for i in reversed(range(self.numRows)):
                    if self.board[i][column] == '0':
                        self.board[i][column] = player
                        self.PrintBoard(self.board)
                        self.count = self.count+1
                        # print("Def Found")
                        return True


                        # return True

                if not found:
                    self.PrintBoard(self.board)
                    # print("Not Found")
                    return False
            self.PrintBoard(self.board )
        except ValueError:
            pass

    def PrintBoard(self,paramboard):

        for i in range(self.numRows):
            forstr = ""
            for j in range(self.numCols):
                forstr = forstr + "     " + paramboard[i][j]
                ttk.Label(self.mainframe, text=forstr, font='helvetica 36').grid(column=1, row=3 + i, sticky=W, )

    def run(self):
        self.PrintBoard(self.board)
        #starts cursor on the input box
        self.drop_entry.focus()

        #if mouse button left or return is pressed, code piece in input box is dropped
        self.root.bind('<Button-1>', self.drop_Piece())
        self.root.bind('<Return>', self.drop_Piece)

        self.root.mainloop()

