from GameBoard import GameBoard
from random import randint

def play_game(player):
    print("Player {}'s turn".format(player))
    game_board.print_board()
    found = False
    col_selection = 0

    while not found:
        game_board.dest = randint(0, 20)
        # if destr ==1 lets you drop a piece
        if game_board.dest == 1:
            try:
                piece_to_place = int(input("Power up- Remove Piece. Select column: "))
                col_selection = piece_to_place
                if (piece_to_place <= 0) or (piece_to_place > game_board.numCols):
                    print("Invalid Input. Pick a number [1, %d]" %game_board.numCols)
                else:
                    found = game_board.remove_piece(piece_to_place - 1, player)
                    game_board.dest = 0
                    print("Removed piece in column", piece_to_place)
                    game_board.print_board()
            except:
                print("Invalid Input. Pick a number [1, %d]" %game_board.numCols)
            if (col_selection > 0) and (col_selection <= game_board.numCols) and found is False:
                print("Column Full. Please pick another column")

        try:
            piece_to_place = int(input("Select column: "))
            col_selection = piece_to_place
            if (piece_to_place <= 0) or (piece_to_place > game_board.numCols):
                print("Invalid Input. Pick a number [1, %d]" %game_board.numCols)
            else:
                found = game_board.drop_piece(piece_to_place - 1, player)
        except:
            print("Invalid Input. Pick a number [1, %d]" %game_board.numCols)
        if (col_selection > 0) and (col_selection <= game_board.numCols) and found is False:
            print("Column Full. Please pick another column")


def individual_turn(playerId):
    play_game(playerId)
    player_won = game_board.check_for_win(playerId)

    if player_won:
        print(game_board.print_board())
        print('Player {} has won it all!'.format(playerId))
        return False
    else:
        return True


def two_player_game():
    game_not_won = True
    turn_counter = 0

    while game_not_won:
        if turn_counter % 2 == 0:   # First Player
            game_not_won = individual_turn('A')
        else:                       # Second Player
            game_not_won = individual_turn('B')

        turn_counter += 1

        if not game_not_won:
            print('Would you like to play again? (y or n)')
            play_again = str(input('>')).lower()
            if play_again == 'y' or play_again == 'yes':
                game_not_won = True
                if game_board.numCols >= 3 or game_board.numRows >= 3:
                    game_board.numCols = game_board.numCols - 1;
                    game_board.numRows = game_board.numCols - 1;
                    game_board.board = [['0' for col in range(game_board.numCols)] for row in range(game_board.numRows)]
                    turn_counter = 0
                else:
                    print('You have finished the game!!!')


        if turn_counter > (game_board.numCols * game_board.numRows) - 1:
            game_board.print_board()
            break


def three_player_game():
    game_not_won = True
    turn_counter = 0

    while game_not_won:
        if turn_counter % 3 == 0:
            game_not_won = individual_turn('A')
        elif turn_counter % 3 == 1:
            game_not_won = individual_turn('B')
        else:
            game_not_won = individual_turn('C')

        turn_counter += 1

        if not game_not_won:
            print('Would you like to play again? (y or n)')
            play_again = str(input('>')).lower()
            if play_again == 'y' or play_again == 'yes':
                game_not_won = True
                if game_board.numCols >= 3 or game_board.numRows >= 3:
                    game_board.numCols = game_board.numCols - 1;
                    game_board.numRows = game_board.numCols - 1;
                    game_board.board = [['0' for col in range(game_board.numCols)] for row in range(game_board.numRows)]
                    turn_counter = 0
                else:
                    print('You have finished the game!!!')

        if turn_counter > (game_board.numCols * game_board.numRows) - 1:
            game_board.print_board()
            break


def run_game():

    if num_players == '2':
        two_player_game()
    elif num_players == '3':
        three_player_game()
    else:
        print('Invalid input, starting two player game')
        two_player_game()


if __name__ == "__main__":
    print('How many players? ')
    num_players = str(input('>'))
    print('How do you want the game board: 1. Default 2. Custom')
    choice = str(input('>'))
    if choice == '2':
        print('Enter the number of rows and columns')
        rows = int(input('>'))
        cols = int(input('>'))
        game_board = GameBoard(rows, cols)
    else:
        game_board = GameBoard()

    run_game()
