"""
The Gameboard class will initialize the Gameboard using a 2D array.
numRows and numCols are set as member variables, so the Gameboard size
is universal throughout the program, but still mutable.
"""
class GameBoard:

    def __init__(self, numRows = 6, numCols = 7):
        self.numRows = numRows
        self.numCols = numCols
        self.dest = 0

        # Build a 2D matrix of a given size filled with all zeros
        self.board = [['0' for col in range(self.numCols)] for row in range(self.numRows)]

    # drop_piece will take the column a user picks and scan it for empty cells.
    # It will then drop it on the lowest available empty cell.
    def drop_piece(self, column, player):
        found = False
        # change the value of the board at position column[j][i]
        if column < self.numCols:
            for i in reversed(range(self.numRows)):
                if self.board[i][column] == '0':
                    self.board[i][column] = player
                    return True

            if not found:
                return False

    # destroys the top piece of any column, replaces it with players piece
    # if player's piece is the top piece of a column, places players piece on the row above column
    def remove_piece(self, column, player):
        found = False
        if column < self.numCols:
            for i in reversed(range(self.numRows)):
                if self.board[i-1][column] == '0':
                    if self.board[i][column] != player:
                        self.board[i][column] = player
                        return True
                    else:
                        self.board[i - 1][column] == player
                        return True
            if not found:
                return False

    # Print our game board in a readable format on the command line.
    def print_board(self):
        print('\n'.join([''.join(['{:4}'.format(item) for item in row])
                         for row in self.board]))

    # check_for_win will scan the board and check for 4 of the same pieces in a row
    # either horizontally, vertically, or diagonally.
    def check_for_win(self, player):  # Must use a 6x7 board size
        try:
            # Checking for horizontal wins
            for y in range(self.numRows-3):
                for x in range(self.numCols-1):
                    if self.board[x][y] == player and self.board[x][y+1] == player and self.board[x][y+2] == player \
                            and self.board[x][y+3] == player:
                        return True

            # Checking for vertical wins
            for x in range(self.numCols-3):
                for y in range(self.numRows-1):
                    if self.board[x][y] == player and self.board[x+1][y] == player and self.board[x+2][y] == player \
                            and self.board[x+3][y] == player:
                        return True

            # Checking for ascending diagonal wins
            for x in range(3, self.numCols-1):
                for y in range(self.numRows-3):
                    if self.board[x][y] == player and self.board[x-1][y+1] == player \
                            and self.board[x-2][y+2] == player and self.board[x-3][y+3] == player:
                        return True

            # Checking for descending diagonal wins
            for x in range(self.numCols-1):
                for y in range(self.numRows):
                    if self.board[x][y] == player and self.board[x-1][y-1] == player \
                            and self.board[x-2][y-2] == player and self.board[x-3][y-3] == player:
                        return True

            return False

        except IndexError:
            print('Out of range, invalid input.')


