# Temporary Test File for demonstration purposes
import unittest
import controller
from unittest.mock import patch
from GameBoard import GameBoard


class TestMove(unittest.TestCase):
    # Creates Gameboard.
    def setUp(self):
        self.board_object = GameBoard()

    # Checks that the board was changed to the proper size.
    def test_size_of_board_col(self):
        self.assertEqual(self.board_object.numCols, 7)

    # Cheks that a piece drops to the bottommost available spot.
    def test_placing_a_piece(self):
        self.board_object.drop_piece(0, 'A')
        test_board = [['0' for col in range(self.board_object.numCols)] for row in range(self.board_object.numRows)]
        test_board[5][0] = 'A'

        self.assertEqual(self.board_object.board, test_board)

    # Checks that the proper piece is removed when calling the remove_piece function.
    def test_removing_a_piece(self):
        self.board_object.remove_piece(0, 'A')
        test_board = [['0' for col in range(self.board_object.numCols)] for row in range(self.board_object.numRows)]
        test_board[5][0] = 'A'

        self.assertEqual(self.board_object.board, test_board)

    # Checks that when a column is fill, it doesn't allow any more pieces
    # to be placed on that column.
    def test_column_overflow(self):
        self.board_object.drop_piece(0, 'A')
        self.board_object.drop_piece(0, 'A')
        self.board_object.drop_piece(0, 'A')
        self.board_object.drop_piece(0, 'A')
        self.board_object.drop_piece(0, 'A')
        self.board_object.drop_piece(0, 'A')
        self.board_object.drop_piece(0, 'H')
        test_board = [['0' for col in range(self.board_object.numCols)] for row in range(self.board_object.numRows)]
        test_board[5][0] = 'A'
        test_board[4][0] = 'A'
        test_board[3][0] = 'A'
        test_board[2][0] = 'A'
        test_board[1][0] = 'A'
        test_board[0][0] = 'A'

        self.assertEqual(self.board_object.board, test_board)

    @unittest.skip('this is how you skip a test')
    def test_size_of_board_row(self):
        self.assertEqual(self.board_object.numRows, 6)

    # Compares an empty board to the initial game board.
    def test_empty_board(self):
        test_board = [['0' for col in range(self.board_object.numCols)] for row in range(self.board_object.numRows)]
        self.assertEqual(self.board_object.board, test_board)

    # Checks if after four identical pieces are dropped horizontal to each other,
    # that it will cause the player to win.
    def test_win_horizontal(self):
        self.board_object.drop_piece(0, 'A')
        self.board_object.drop_piece(1, 'A')
        self.board_object.drop_piece(2, 'A')
        self.board_object.drop_piece(3, 'A')

        self.assertTrue(self.board_object.check_for_win('A'))

    # Checks if after four identical pieces are dropped vertical to each other,
    # that it will cause the player to win.
    def test_win_vertical(self):
        self.board_object.drop_piece(0, 'A')
        self.board_object.drop_piece(0, 'A')
        self.board_object.drop_piece(0, 'A')
        self.board_object.drop_piece(0, 'A')

        self.assertTrue(self.board_object.check_for_win('A'))

    # Checks if after four identical pieces are dropped diagonally ascending
    #  to each other, that it will cause the player to win.
    def test_win_diagonal_ascending(self):
        self.board_object.drop_piece(0, 'A')  # Col 1

        self.board_object.drop_piece(1, 'B')  # Col 2
        self.board_object.drop_piece(1, 'A')

        self.board_object.drop_piece(2, 'B')  # Col 3
        self.board_object.drop_piece(2, 'B')
        self.board_object.drop_piece(2, 'A')

        self.board_object.drop_piece(3, 'B')  # Col 4
        self.board_object.drop_piece(3, 'B')
        self.board_object.drop_piece(3, 'B')
        self.board_object.drop_piece(3, 'A')

        self.assertTrue(self.board_object.check_for_win('A'))

    # @unittest.skip('Broken test')
    def test_win_diagonal_descending(self):
        self.board_object.drop_piece(0, 'B')  # Col 1
        self.board_object.drop_piece(0, 'B')
        self.board_object.drop_piece(0, 'B')
        self.board_object.drop_piece(0, 'A')

        self.board_object.drop_piece(1, 'B')  # Col 2
        self.board_object.drop_piece(1, 'B')
        self.board_object.drop_piece(1, 'A')

        self.board_object.drop_piece(2, 'B')  # Col 3
        self.board_object.drop_piece(2, 'A')

        self.board_object.drop_piece(3, 'A')  # Col 4

        self.assertTrue(self.board_object.check_for_win('A'))
        self.assertFalse(self.board_object.check_for_win('B'))

    @unittest.skip('Unable to test, too complex of scoping of GameBoard')
    @patch('builtins.input', side_effects=['3', '1', '2', '3'])
    def test_playing_with_three_players(self, input):
        game_board = self.board_object
        controller.run_game()
        existing_board = self.board_object.print_board()

        test_board = [['0' for col in range(self.board_object.numCols)] for row in range(self.board_object.numRows)]
        test_board[0][0] = 'A'
        test_board[1][0] = 'B'
        test_board[2][0] = 'C'

        self.assertEqual(test_board, existing_board)


if __name__ == '__main__':
    unittest.main()
